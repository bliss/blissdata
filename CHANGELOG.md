# CHANGELOG.md

## 2.0.0
New features:
- CursorGroup replaces StreamingClient to listen multiple streams at once.
- Cursor and CursorGroup now accept any kind of stream (aka BaseStream), not just pure Redis ones.
- Cursor and CursorGroup no longer return raw data, but a View object corresponding to a particular range of points.
- Views may return Redis content as before, or embed any custom de-referencing mechanism to grab data from another storage.
- Blissdata can be extended by plugin packages to include new types of stream and views.
- Plugins automatic discovery through python entry_points.
- Add dtype and shape properties to any stream
- Add an h5py-like API based on Redis to browse HDF5 while scan is running
- Enable publication of a scan's streams from multiple processes. Done by adding Scan.get_writer_stream method to pick read-write stream object from a loaded scan.
- Streams have a new .wait_seal(timeout) method.
- Enable type checking support (PEP561) for mypy

Changes:
- Drop python 3.9
- Drop pydantic V1
- StreamingClient is removed (see CursorGroup)
- Multiple high-level streams are now included in blissdata as built-in plugins (lima, lima2, hdf5 fallback, scan sequence)
- scan_cls argument in DataStore.load_scan() is removed. Accessing bare event streams inside LimaStream for example is no longer needed.
- DataStore.create_scan() takes a generic StreamDefinition object as argument. Each plugin can define a make_definition(...) factory method with custom arguments.
- Low level encoder and decoders (Numpy array interface protocol and Json) are now merged into a single encoder object with both encode/decode methods.
- Support new silx version that exposes is_h5py_exception.

Bug fixes:
- Fix numpy scalar jsonifying in scan info.
- Make unsealed streams accept 'None' terminated slices (instead of raising IndexNotYetThereError).
- Fix beacon client to use with beacon external redis db (new beacon feature).

## 1.1.4
Changes:
- Support new silx version that exposes is_h5py_exception.

Bug fixes:
- Fix beacon client to use with beacon external redis db (new beacon feature).

## 1.1.3
Changes:
- Reorganise project: new repository, migrate to pyproject.toml, use 'src' layout.

Bug fixes:
- Fix numpy scalar jsonifying in scan info.
- Prevent use of h5py>=3.12 because of file locking breaking changes.
- Update redis-om and pydantic version requirements.

## 1.1.2
Bug fixes:
- Fix stream model incompatibility between pydantic v1 and v2
- Several fixes for h5api module  https://gitlab.esrf.fr/bliss/bliss/-/merge_requests/6611

New features:
- Add support for key-value service in bliss beacon server
- Add lima2 client
- Gevent based stream sinks now put names on their greenlets

## 1.1.1
Bug fixes:
- Fix race condition on stream.__getitem__ that could raise IndexNoMoreThereError instead of IndexNotYetThereError
- Fix deprecation warning on silx.third_party.EdfFile

## 1.1.0
New features:
- h5papi.dynamic_hdf5: accept custom `Lima` path template parameters.
- h5papi.dynamic_hdf5: added the `filename` attribute.
- Support `Lima` data frame v4.

Changes:
- Support `redis-om>=0.3` versions with pydantic v1 and v2 support, in addition to `redis-om<0.3` versions with pydantic v1 support only.
- Unit tests: `pytest_redis` fixture API change and Redis module support.
- Remove `h5py` upper version bound.
- Remove `python` upper version bound.
- Scan loading from Redis can raise `ScanValidationError` in addition to `ScanNotFoundError`.
- `ScanValidationError` and `ScanNotFoundError` have a common base exception `ScanLoadError`.

Bug fixes:
- Re-establish beacon connection in case of server restart.

## 1.0.6
Changes:
- Support new silx version that exposes is_h5py_exception.

Bug fixes:
- Fix beacon client to use with beacon external redis db (new beacon feature).

## 1.0.5
Changes:
- Reorganise project: new repository, migrate to pyproject.toml, use 'src' layout.

Bug fixes:
- Fix numpy scalar jsonifying in scan info.
- Prevent use of numpy>=2 because of h5py 3.9.0 incompatibility.
- Repair memory_tracker test issue caused by unrelated warning messages.

## 1.0.4
Changes:
- Support `redis-om>=0.3` versions with pydantic v1 and v2 support, in addition to `redis-om<0.3` versions with pydantic v1 support only.
- Unit tests: `pytest_redis` fixture API change.

## 1.0.3
Changes:
- Calling stream.seal() multiple times no longer raise exception.

Bug fixes:
- h5papi.dynamic_hdf5: Fix race conditions when handling the end of scans

## 1.0.2
New features:
- An exception is raised when trying to push more than 1MB in scan's JSON (scan's large static artifacts needs a proper mechanism)

Bug fixes:
- Python 3.7 is not supported

## 1.0.1
New features:
- Memory tracker can initialize content of a fresh Redis DB with --init-db option

Bug fixes:
- LimaClient: Fixed invalid calculation of the index for the last file readable image.
- LimaClient: Open EDF files in read-only mode
- LimaClient: Do not use 'readImage(-1)' on lima-tango-server<1.10 (ask for 'last_image_ready' instead)
- Tests: Running blissdata tests no longer dumps .rdb file

## 1.0.0
New features:
- New internal data structure based on RedisJson and RediSearch (see documentation)
- New API with datastore, scans and streams (see documentation)

## 0.3.3

New features:
- Allow creation of ordered hash from a hash setting content in Redis

Bug fixes:
- Fix missing gevent submodules import

## 0.3.2

Bug fixes:
- Python 3.10 and higher are not supported

## 0.3.1

Bug fixes:
- Allow h5py 3.6 and higher

## 0.3.0

New features:
- `h5py`-like API for
    - static HDF5 files (file content does not change while reading)
    - dynamic HDF5 files (file content changes while reading)

## 0.2.0

New features:
- `RemoteNodeWalker` for non-gevent readers (gevent is still used in a subprocess)

## 0.1.2

Bug fixes:
- pytango is optional

## 0.1.1

Bug fixes:
- yaml config URL parsing fails for filename on Windows

## 0.1.0

New features:
- Access to all data produced by Bliss acquisitions (Redis database 1)
- Access to all public device settings (Redis database 0)
- Access to beamline configuration (Beacon server)

The Redis related code has been extracted from the *Bliss* project without
changing the API.
