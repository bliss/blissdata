"""Few general purpose utils"""


class UndefinedType:
    __slots__ = []


Undefined = UndefinedType()
