from .stream import Lima2Stream, Lima2View  # noqa: F401

stream_cls = Lima2Stream
view_cls = Lima2View
