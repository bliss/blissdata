from .stream import ScanStream, ScanView  # noqa: F401

stream_cls = ScanStream
view_cls = ScanView
