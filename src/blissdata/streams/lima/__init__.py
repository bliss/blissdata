from .stream import LimaStream, LimaView  # noqa: F401

stream_cls = LimaStream
view_cls = LimaView
