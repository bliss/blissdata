# Blissdata

## **Welcome to the Blissdata documentation!**

Blissdata is a python library designed to store and access scientific data
during and after experiment.

![Blissdata concept](assets/blissdata_box.png){ width="400" align='right'}
It relies on a Redis database which functions as both a storage and a
streaming engine. This approach offers both real-time and historical access
capabilities.

Storage can be further extended thanks to plugins. It is thus possible to define
references or fallback mechanism to grab archived data from files or other
services.

This guide will walk you through the installation and configuration of Blissdata
and its Redis database. In a second time, you will see how to use Blissdata for
publication and live access to your data.

<!-- Moreover, a HDF5 wrapper is currently in development to mimic
[h5py](https://docs.h5py.org/en/stable/index.html) API to run scans and to make
already written scripts relying on files reusable. -->
