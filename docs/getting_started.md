# Getting started

Blissdata is articulated around a few concepts.

## DataStore

The [**DataStore**](datastore.md) corresponds to a Redis database. Most of time you have one,
but we could imagine cases where one need to monitor multiple instances.

## Scans

Inside [**DataStore**](datastore.md) are [**Scans**](scans.md), each representing an acquisition. It has
a state telling about its progress, metadata to store any scientifically
relevant description and a bunch of [**Streams**](streams.md).

## Streams

[**Streams**](streams.md) are the real messengers that carry the acquired data. Each has a
well defined data type (complex object streams can be achieved with plugins).
It behaves both like an array and a socket, you can pick an index or wait for
upcoming values.

---

The following sections will explore each of these building blocks in more detail.
